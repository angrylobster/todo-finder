## Description
TodoFinder is a Node.js based application that searches for files that contain the text string "TODO".

When run, TodoFinder will recursively search all files and folders beginning from **a relative path** determined by the user. The application will also seek the user's permissions to proceed with the search and reading of files at each step. This is to prevent the unwanted reading of a huge number of files.

## Requirements
**[Node.js](https://nodejs.org/en/download/)** - It is recommended that you have v10 and above installed.

**[npm](https://www.npmjs.com/)** - You will need a package manager to install some dependencies.

## Instructions
#### Cloning the application
You can clone this application from the repository at: https://gitlab.com/angrylobster/todo-finder

#### Installing dependencies
Install all dependencies using `npm install`.

#### Initializing the application
TodoFinder can be initialized with `npm run start` or `node main.js`.

#### Choosing a relative search path
Once TodoFinder has initialized, you should see a prompt that looks like this: 
```
  _____         _       _____ _           _           
 |_   _|__   __| | ___ |  ___(_)_ __   __| | ___ _ __ 
   | |/ _ \ / _` |/ _ \| |_  | | '_ \ / _` |/ _ \ '__|
   | | (_) | (_| | (_) |  _| | | | | | (_| |  __/ |   
   |_|\___/ \__,_|\___/|_|   |_|_| |_|\__,_|\___|_|   
                                                      
Please enter the relative path you want to search: 
```

You will be required to input a relative path where you would like the app to begin searching for files containing the word `TODO`. For example, if you cloned the application to `/home/applications/todo-finder`, and you would like to search for all files containing `TODO` at `/home/applications`, then you will need to key in the relative path `..`

Once a search path has been selected, you will see a prompt confirming the search path, and asking for your permission to begin a file search:

```
Search path set to: /home/applications
Do you wish to begin searching for files? 
```

If you chose an invalid search path, then the application will immediately terminate: 

```
/home/applications/garbage-in-garbage-out is not a valid search path
Terminating the application.
```

If the application terminates, you will need to re-initialize it (see the step above).

#### Begin searching for files
To begin searching for files, you will need to input a `y` or `yes`. The application will begin logging the files and sub-directories. Because the application searches for files in folders recursively, this process may take a long time! 

> **Warning:** If you find that the file search process is going on for too long, you can press the keystrokes `Ctrl + C` or `CMD + C` twice to terminate the application

Once the search has been completed, a `total` count of the files found will be presented:

```
Found file at: /home/applications/hello-world/index.js
Checking sub-directory: /home/applications/hello-world/src
Found file at: /home/applications/hello-world/src/app.js
A total of 2 files were found.
Do you wish to search the files for the matching string 'TODO'?
```

If you do not input an affirmative answer, the application will terminate at this step: 

```
Do you wish to begin searching for files? 
User did not confirm file search.
Terminating the application.
```

#### Begin reading the files
You can input another `y` or `yes` to begin reading the files that were found. Each file is being searched for the `TODO` string. Once all the files have been read, you should see some logs displaying the files that were read, as well as the total number of files that were found to contain `TODO`. You will also be asked whether you wish to view the results:

```
Reading file at: /home/applications/hello-world/index.js
Reading file at: /home/applications/hello-world/src/main.js
Reading file at: /home/applications/hello-world/src/services/index.js
3 files containing 'TODO' were found.
Do you wish to view the results? 
```

#### View the results
Finally, you can input the last `y` or `yes` to view a list of the paths containing the files which contain `TODO`:

```
/home/applications/hello-world/index.js
/home/applications/hello-world/src/main.js
/home/applications/hello-world/src/services/index.js
Job completed.
Terminating the application.
```