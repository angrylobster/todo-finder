const TodoFinder = require('./src/TodoFinder');
const finder = new TodoFinder(__dirname);

process.on('SIGINT', () => {
    process.exit(1);
});

finder.init();