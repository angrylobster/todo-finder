const fileService = require('../../src/services/fileService');
const path = require('path');

function buildPath (...relativePaths) {
    return path.join(__dirname, ...relativePaths);
}

describe('fileService', () => {
    describe('.getFilePaths()', () => {
        const PATHS = {
            SINGLE_FILE: buildPath('..', '/data/file'),
            ONLY_FOLDERS: buildPath('..', '/data/folder'),
            NESTED_FILES: buildPath('..', '/data/filesAndSubfolders'),
        };
        
        it('should return a file path if there is a file at that path', async () => {
            const [filePath] = await fileService.getFilePaths(PATHS.SINGLE_FILE);
            expect(filePath).toEqual(PATHS.SINGLE_FILE + '/file.js');
        });

        it('should ignore folders', async () => {
            const results = await fileService.getFilePaths(PATHS.ONLY_FOLDERS);
            expect(results).toEqual([]);
        });

        it('should return files paths recursively', async () => {
            const results = await fileService.getFilePaths(PATHS.NESTED_FILES);
            expect(results.length).toEqual(4);
        });
    });

    describe('.fileContainsString()', () => {
        const TODO_STRING = 'TODO';
        const PATHS = {
            TODO_FILE: buildPath('..', '/data/file/file.js'),
            NO_TODO_FILE: buildPath('..', '/data/filesAndSubfolders/folder/file.js'),
        };

        it('should return true if the file contains the search string', async () => {
            const containsTodo = await fileService.fileContainsString(PATHS.TODO_FILE, TODO_STRING);
            expect(containsTodo).toEqual(true);
        });

        it('should return false if the file does not contain the search string', async () => {
            const containsTodo = await fileService.fileContainsString(PATHS.NO_TODO_FILE, TODO_STRING);
            expect(containsTodo).toEqual(false);
        });
    });
});