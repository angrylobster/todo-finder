const pathUtil = require('../../src/utils/path');
const sha256 = require('sha256');

describe('pathUtil', () => {
    describe('.isValidPath()', () => {
        it('should return true if the relative path is a valid path', async () => {
            expect(await pathUtil.isValidPath('.')).toEqual(true);
        });

        it('should return false if the relative path is not a valid path', async () => {
            expect(await pathUtil.isValidPath(sha256(Math.random() * 10000000 + ''))).toEqual(false);
            expect(await pathUtil.isValidPath(__dirname + '/banana')).toEqual(false);
        });
    });
});