const fileService = require('./services/fileService');
const figlet = require('figlet');
const chalk = require('chalk');
const readline = require('readline');
const pathUtil = require('./utils/path');
const path = require('path');

const ASCII_ART = {
    COLOR: 'green',
    FIGLET_OPTIONS: {
        font: 'Standard',
        width: 100,
    },
};
const READLINE_INTERFACE_OPTIONS = {
    input: process.stdin,
    output: process.stdout,
};
const CONFIRMATION_RESPONSES = ['yes', 'y', 'true', 'affirmative', 'confirm'];

module.exports = class TodoFinder {
    constructor (rootPath) {
        if (!rootPath) throw new Error('You must declare the run path!');
        this.rootPath = rootPath;
        this.searchPath = '';
        this.readlineInterface = readline.createInterface(READLINE_INTERFACE_OPTIONS);
        this.filePaths = [];
        this.matchingFileIndices = [];
        this.searchString = 'TODO';
    }

    printAsciiWord (word) {
        console.log(chalk[ASCII_ART.COLOR](figlet.textSync(word, ASCII_ART.FIGLET_OPTIONS)));
    }

    getUserInput (question) {
        return new Promise(resolve => {
            this.readlineInterface.question(question, answer => resolve(answer));
        });
    }

    isUserInputAffirmative (userInput) {
        return CONFIRMATION_RESPONSES.includes(userInput.toLowerCase());
    }

    getAbsolutePath (relativeSearchPath) {
        return path.resolve(this.rootPath, relativeSearchPath);
    }
    
    async searchFiles () {
        this.filePaths = await fileService.getFilePaths(this.searchPath);
        console.log(`A total of ${this.filePaths.length} files were found.`);
    }

    async readFiles () {
        this.matchingFileIndices = (await Promise.all(
            this.filePaths.map(filePath => {
                return fileService.fileContainsString(filePath, this.searchString);
            }),
        )).reduce((results, hasMatch, index) => {
            if (hasMatch) results.push(index);
            return results;
        }, []);
        console.log(`${this.matchingFileIndices.length} files containing '${this.searchString}' were found.`);
    }

    displayResults () {
        this.matchingFileIndices.forEach(index => {
            console.log(this.filePaths[index]);
        });
    }

    terminateApplication (extraMessage) {
        if (extraMessage) console.log(extraMessage);
        console.log('Terminating the application.');
        process.exit();
    }

    setSearchPath (path) {
        this.searchPath = path;
        console.log('Search path set to:', path);
    }

    async init () {
        this.printAsciiWord(this.constructor.name);
        
        const searchPath = await this.getUserInput('Please enter the relative path you want to search: ');
        const fullSearchPath = this.getAbsolutePath(searchPath);

        if (await pathUtil.isValidPath(fullSearchPath)) this.setSearchPath(fullSearchPath);
        else this.terminateApplication(`${fullSearchPath} is not a valid search path`);

        const searchFilesInput = await this.getUserInput('Do you wish to begin searching for files? ');

        if (this.isUserInputAffirmative(searchFilesInput)) await this.searchFiles();
        else this.terminateApplication('User did not confirm file search.');

        const readFilesInput = await this.getUserInput(
            `Do you wish to search the files for the matching string '${this.searchString}'? `,
        );

        if (this.isUserInputAffirmative(readFilesInput)) await this.readFiles();
        else this.terminateApplication('User did not confirm file read.');

        const displayResultsInput = await this.getUserInput('Do you wish to view the results? ');

        if (this.isUserInputAffirmative(displayResultsInput)) await this.displayResults();
        else this.terminateApplication('User did not confirm result display.');
        
        this.terminateApplication('Job completed.');
    }
};
