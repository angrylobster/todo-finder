const fsPromises  = require('fs/promises');
const fs = require('fs');

function buildFilePath (path, fileName) {
    return `${path}/${fileName}`;
}

module.exports = {
    async getFilePaths (path) {
        const dirents = await fsPromises.readdir(path, { withFileTypes: true });
        const filePaths = [];
        for (const dirent of dirents) {
            if (dirent.isFile()) {
                console.log('Found file at:', buildFilePath(path, dirent.name));
                filePaths.push(buildFilePath(path, dirent.name));
            } else if (dirent.isDirectory()) {
                console.log('Checking sub-directory:', buildFilePath(path, dirent.name));
                filePaths.push(...await this.getFilePaths(buildFilePath(path, dirent.name)));
            }
        }
        return filePaths;
    },
    fileContainsString (filePath, searchString) {
        return new Promise(resolve => {
            const fileStream = fs.createReadStream(filePath);
            console.log('Reading file at:', filePath);
            fileStream.on('data', chunk => {
                if (chunk.toString().includes(searchString)) {
                    fileStream.destroy();
                    resolve(true);
                }
            });
            fileStream.on('end', () => resolve(false));
        });
    },
};