const fs = require('fs');

module.exports = {
    isValidPath (path) {
        return new Promise(resolve => {
            fs.stat(path, (err, stats) => {
                if (err) resolve(false);
                else resolve(stats.isDirectory());
            });
        });
    },
};