module.exports = {
    'env': {
        'commonjs': true,
        'es2020': true,
        'node': true,
        'jest': true,
    },
    'extends': 'eslint:recommended',
    'parserOptions': {
        'ecmaVersion': 11,
    },
    'rules': {
        'indent': [
            'error',
            4,
        ],
        'linebreak-style': [
            'error',
            'unix',
        ],
        'quotes': [
            'error',
            'single',
        ],
        'semi': [
            'error',
            'always',
        ],
        'object-curly-spacing': ['error', 'always'],
        'comma-dangle': ['error', 'always-multiline'],
        'space-before-function-paren': ['error', 'always'],
        'space-before-blocks': ['error', 'always'],
        'brace-style': ['error', '1tbs', { allowSingleLine: true }],
        'object-curly-newline': [
            'error',
            {
                ObjectExpression: {
                    consistent: true,
                    multiline: true,
                },
                ObjectPattern: {
                    multiline: true,
                    consistent: true,
                },
            },
        ],
        'max-len': ['error', { code: 120 }],
    },
};
